<?php
session_start();
?>
<html>
<head>
<style>
  #sortable { 
  list-style-type: none;
  }
#sortable li{
	display:inline;
	width:100%;
}
  .cell{
  display:table-cell;
  border:1px solid black;
  width:168px;
  text-align:center;
  }
  .table {
    width: 100%;
    border-collapse: collapse;
	display:table;
}

.table, .cell{
    border: 1px solid black;
    padding: 5px;
}
.allrows{
	display:table-row;
	width:100%;
}
.Logged{
	width:250px;
	padding:5px;
}
body{
	margin:0;
}
</style>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#sortable" ).sortable();//Drag & Drop рядків
	$("#showdel").click(function(){//Показати селектор рядку
		$("#delete").toggle();
		$("#etext").text("");
	});
	$("#showadd").click(function(){// показує форму для додавання рядку
		$("#add").toggle();
	});
	$(".adds").click(function(){// функція для додавання рядку в базу даних
	 $.ajax({
      url: 'add.php', 
      type: 'POST', 
      dataType: 'html',
      data: {d: $("#date").val(),t: $("#time").val(),n: $("#name").val(),p: $("#product").val(),a: $("#amount").val(),pr: $("#price").val()}
});
	});
	$("#sort").click(function(){//Сортування таблиці
	 $.ajax({
      url: 'sort.php', 
      type: 'POST', 
      dataType: 'html',
      data: {r: $('#radio_form input:radio:checked').val()},
	  success: function(json){ 
        $("#sortable").html(json) 
      }
});
});
  } );
  
  function ed(){//функція для редагування рядку
	  var ii = $("#eid").attr('placeholder');
	  if($("#eid").val()!=""){
		 ii = $("#eid").val();
	  }
	  var dd = $("#edate").val();
	  if(dd==""){
		 dd = $("#date").attr('placeholder');
	  }
	  var tt = $("#etime").val();
	  if(tt==""){
		 tt = $("#etime").attr('placeholder');
	  }
	  var nn = $("#ename").val();
	  if(nn==""){
		 nn = $("#ename").attr('placeholder');
	  }
	  var pp = $("#eproduct").val();
	  if(pp==""){
		 pp = $("#eproduct").attr('placeholder');
	  }
	  var aa = $("#eamount").val();
	  if(aa==""){
		 aa = $("#eamount").attr('placeholder');
	  }
	  var ppr = $("#eprice").val();
	  if(ppr==""){
		 ppr = $("#eprice").attr('placeholder');
	  }
	  		  var s = document.getElementById("Selected").options.selectedIndex;//отримуємо номер вибраної опції селектора
	   var set = document.getElementById("Selected").options[s].text;//отримуємо текст опції тобто id рядка
/*заміна значень вибраного рядка на нові*/ document.getElementById("row"+set).innerHTML = "<div id='row"+set+"' class='allrows'><div class='cell'>" +ii+ "</div><div class='cell'>" + document.getElementById('edate').value + "</div><div class='cell'>" +document.getElementById('etime').value+ "</div><div class='cell'>" +document.getElementById('ename').value+ "</div><div class='cell'>" +document.getElementById('eproduct').value+ "</div><div class='cell'>" +document.getElementById('eamount').value+ "</div><div class='cell'>" +document.getElementById('eprice').value + "</div></div>";
      
	  if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("exd").innerHTML = this.responseText;
            }
        };
        xmlhttp.open("GET","edit.php?i="+ii+"&d="+dd+"&t="+tt+"&n="+nn+"&p="+pp+"&a="+aa+"&pr="+ppr,true);
        xmlhttp.send();
  }
function press(){//додає новий рядок в таблицю
	  var last = parseInt(document.getElementById("Selected").options[document.getElementById("Selected").length-1].text)+1;// вибір id останнього рядка в таблиці
var list = document.getElementById('sortable');
var li = document.createElement('li');

li.innerHTML="<div id='row"+last+"' class='allrows'><div class='cell'>" +last+ "</div><div class='cell'>" + document.getElementById('date').value + "</div><div class='cell'>" +document.getElementById('time').value+ "</div><div class='cell'>" +document.getElementById('name').value+ "</div><div class='cell'>" +document.getElementById('product').value+ "</div><div class='cell'>" +document.getElementById('amount').value+ "</div><div class='cell'>" +document.getElementById('price').value + "</div></div>";
list.appendChild(li);//додавання нового рядка в таблицю
var newopt = document.createElement('option');
newopt.innerHTML=last;
document.getElementById('Selected').appendChild(newopt);//додавання нової опції в селектор
localStorage.setItem("id"+last+"", last);// запис даних в LocalStorage
	localStorage.setItem("date"+last+"", document.getElementById('date').value);
	localStorage.setItem("time"+last+"", document.getElementById('time').value);
	localStorage.setItem("name"+last+"", document.getElementById('name').value);
	localStorage.setItem("product"+last+"", document.getElementById('product').value);
	localStorage.setItem("amount"+last+"", document.getElementById('amount').value);
	localStorage.setItem("price"+last+"", document.getElementById('price').value);
	
}

function showedit(){//показує форму для редагування
$("#etext").toggle();
	var s = document.getElementById("Selected").options.selectedIndex;
	   var set = document.getElementById("Selected").options[s].text;
	   if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("etext").innerHTML = this.responseText;
            }
        };
        xmlhttp.open("GET","showedit.php?q="+set,true);
        xmlhttp.send();
}
  
   function deleterow() {//функція видалення з таблиці
var n = document.getElementById("Selected").options.selectedIndex;
var val = document.getElementById("Selected").options[n].text;
var sel = document.getElementById("Selected");
sel.removeChild(document.getElementById("Selected").options[n]);
var Table = document.getElementById('row'+val);//отримуємо id необхідного для видалення рядка
Table.innerHTML = '';//видалення рядка з таблиці
document.getElementById("edit").disabled = true;
document.getElementById("delrow").disabled = true;
localStorage.removeItem("id"+val);
localStorage.removeItem("date"+val);
localStorage.removeItem("time"+val);
localStorage.removeItem("name"+val);
localStorage.removeItem("product"+val);
localStorage.removeItem("amount"+val);
localStorage.removeItem("price"+val);
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("etext").innerHTML = this.responseText;
            }
        };
        xmlhttp.open("GET","delete.php?val="+val,true);
        xmlhttp.send();
    }
	function checkdel(){//активує/деактивує кнопки Edit та Delete
		if(document.getElementById("Selected").options.selectedIndex != 0){
			document.getElementById('delrow').disabled=false;
			document.getElementById('edit').disabled=false;
		}
		else{
			document.getElementById('delrow').disabled=true;
			document.getElementById('edit').disabled=true;
		}
	}
  </script>
</head>
<body onload="document.getElementById('delrow').disabled=true; document.getElementById('edit').disabled=true;">
<div class="Logged"><?php echo " Hello ".$_SESSION['name']." you can "; ?>
<a href="changepass.php">change</a> your password</div>
<div id="txt">
<?php
$con = mysqli_connect('localhost','root','','magnis');//виведення таблиці
if (!$con) {
    die('Could not connect: ' . mysqli_error($con));
}
$sql="SELECT * FROM base";
$result = mysqli_query($con,$sql);

echo "

<ul id='sortable'>
<div class='allrows'>
<div class='cell'>ID</div>
<div class='cell'>Date</div>
<div class='cell'>Time</div>
<div class='cell'>Nickname</div>
<div class='cell'>Product</div>
<div class='cell'>Amount</div>
<div class='cell'>Price</div>
</div>";
while($row = mysqli_fetch_array($result)) {
    echo "<script>localStorage.setItem('id".$row['id']."', '".$row['id']."');
	localStorage.setItem('date".$row['id']."', '".$row['date']."');
	localStorage.setItem('time".$row['id']."', '".$row['time']."');
	localStorage.setItem('name".$row['id']."', '".$row['name']."');
	localStorage.setItem('product".$row['id']."', '".$row['product']."');
	localStorage.setItem('amount".$row['id']."', '".$row['amount']."');
	localStorage.setItem('price".$row['id']."', '".$row['price']."');
	</script><li><div id='row".$row['id']."' class='allrows'>";
    echo "<div class='cell'>" . $row['id'] . "</div>";
    echo "<div class='cell'>" . $row['date'] . "</div>";
    echo "<div class='cell'>" . $row['time'] . "</div>";
    echo "<div class='cell'>" . $row['name'] . "</div>";
	echo "<div class='cell'>" . $row['product'] . "</div>";
    echo "<div class='cell'>" . $row['amount'] . "</div>";
	echo "<div class='cell'>" . $row['price'] . "</div>";
    echo "</div></li>";
}
echo "</ul>";
?>
</div>
<form id="radio_form" onmouseover="disable()">Order by: <input type='radio' name='r' value='SELECT * FROM base ORDER BY ID'>ID ;
<input type='radio' name='r' value='SELECT * FROM base ORDER BY date'>Date ;
<input type='radio' name='r' value='SELECT * FROM base ORDER BY time'>Time ;
<input type='radio' name='r' value='SELECT * FROM base ORDER BY name'>Name ;
<input type='radio' name='r' value='SELECT * FROM base ORDER BY product'>Product ;
<input type='radio' name='r' value='SELECT * FROM base ORDER BY amount'>Amount ;
<input type='radio' name='r' value='SELECT * FROM base ORDER BY price'>Price ;
<input value="Sort" id="sort" class="sort" type="button">
<input value="Delete|Edit" type="button" id="showdel">
<input value="Add" type="button" id="showadd">
</form>
<form>
<div style="display:none;" id="delete">
Select row number to delete
<select onchange="checkdel()" id="Selected">
<option>Select</option>
<?php
$con = mysqli_connect('localhost','root','','magnis');//додавання опцій в селектор
if (!$con) {
    die('Could not connect: ' . mysqli_error($con));
}
$sql="SELECT * FROM base";
$result = mysqli_query($con,$sql);
while($row = mysqli_fetch_array($result)) {
	echo "<option>" . $row['id'] . "</option>";
}
?>
</select>
<input type="button" value="Delete" onclick="deleterow()" id="delrow">
<input type="button" id="edit" value="Show edit form" onclick="showedit()">
</div>
<br>
<div style="display:none;" id="add">
Date<input id="date" name="date"> 
Time<input id="time" name="time"> 
Name<input id="name" name="name"> 
Product<input id="product" name="product"> 
Amount<input id="amount" name="amount"> 
Price<input id="price" name="price"> 
<input onclick="press()" class="adds" type="button" value="Add" ><br>

</div><br>
<div style="display:none;" id="etext">
</div>
<div id="exd">
</div>
</form>
</body>
</html>